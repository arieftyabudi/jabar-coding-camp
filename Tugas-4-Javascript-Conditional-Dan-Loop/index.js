// Soal 1

var nilai = 54;
if (nilai < 55) {
  console.log('E');
} else if (nilai >= 55 && nilai < 65) {
  console.log('D');
} else if (nilai >= 65 && nilai < 75) {
  console.log('C');
} else if (nilai >= 75 && nilai < 85) {
  console.log('B');
} else {
  console.log('A');
}

//Soal 2

var tanggal = 31;
var bulan = 8;
var tahun = 1991;
var lahir = '';
switch (bulan) {
  case 1:
    lahir = 'Januari';
    break;
  case 2:
    lahir = 'Februari';
    break;
  case 3:
    lahir = 'Maret';
    break;
  case 4:
    lahir = 'April';
    break;
  case 5:
    lahir = 'Mei';
    break;
  case 6:
    lahir = 'Juni';
    break;
  case 7:
    lahir = 'Juli';
    break;
  case 8:
    lahir = 'Agustus';
    break;
  case 9:
    lahir = 'September';
    break;
  case 10:
    lahir = 'Oktober';
    break;
  case 11:
    lahir = 'November';
    break;
  case 12:
    lahir = 'Desember';
}
console.log(tanggal + ' ' + lahir + ' ' + tahun);

//Soal 3

var pagar = 7;
var hasil = '';
for (var i = 0; i < pagar; i++) {
  hasil += '*';
  console.log(hasil);
}

//Soal 4

var input = 10;
var job = '';
var teks = '';
var index = 0;
for (var b = 1; b <= input; b++) {
  switch (index) {
    case 0:
      job = 'Programing';
      break;
    case 1:
      job = 'JavaScript';
      break;
    case 2:
      job = 'vue js';
      break;
  }
  console.log(b + ' - I Iove ' + job + ' ');
  index++;

  if (index == 3) {
    for (i = 3; i <= 3; i += 3) {
      teks += '===';
      if (i % 5 != 0) {
        console.log(teks);
      }
    }
    index = 0;
  }
}
