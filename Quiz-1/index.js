//Soal 1

function next_date(hari, bulan, tahun) {
  var tanggalBaru = 1;
  var h = hari;
  var b = bulan;
  var t = tahun;

  function range(mulai, akhir) {
    var array = [];

    var length = akhir - mulai;

    for (var i = 0; i <= length; i++) {
      array[i] = mulai;
      mulai++;
    }

    return array;
  }
  var januari = range(1, 31);
  var februari = range(1, 28);
  var maret = range(1, 31);
  var april = range(1, 30);
  var mei = range(1, 31);
  var juni = range(1, 30);
  var juli = range(1, 31);
  var agustus = range(1, 31);
  var september = range(1, 30);
  var oktober = range(1, 31);
  var november = range(1, 30);
  var desember = range(1, 31);

  var kalender = {
    1: { month: 'Januari', tanggal: januari },

    2: { month: 'Februari', tanggal: februari },

    3: { month: 'Maret', tanggal: maret },

    4: { month: 'April', tanggal: april },

    5: { month: 'Mei', tanggal: mei },

    6: { month: 'Juni', tanggal: juni },

    7: { month: 'Juli', tanggal: juli },

    8: { month: 'Agustus', tanggal: agustus },

    9: { month: 'September', tanggal: september },

    10: { month: 'Oktober', tanggal: oktober },

    11: { month: 'November', tanggal: november },

    12: { month: 'Desember', tanggal: desember },
  };

  var pembanding = kalender[b].tanggal.slice(-1).toLocaleString();
  var akhirTahun1 = kalender[12].month;
  var akhirTahun2 = kalender[12].tanggal[30];

  if (
    kalender[b].month == akhirTahun1 &&
    kalender[b].tanggal[h - 1] == akhirTahun2
  ) {
    b = 1;
    h = tanggalBaru;
    t += 1;
  } else if (pembanding == 31 || pembanding == 30 || pembanding == 28) {
    b += 1;
    h = tanggalBaru;
  } else {
    h += 1;
  }
  console.log(pembanding);
  console.log(kalender[b].tanggal[h - 1] + ' ' + kalender[b].month + ' ' + t);
}
next_date(31, 1, 2000);

//Soal 2

function jumlah_kata(kata) {
  var pisah = kata.split(' ').filter(function (spasi) {
    return spasi != '';
  });

  return pisah.length;
}

console.log(jumlah_kata('Halo   nama saya Muhammad Iqbal Mubarok'));
