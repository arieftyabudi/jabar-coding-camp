//Soal 1

var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];
var urutkan = daftarHewan.sort();
var jawab = '';

for (var i = 0; i < daftarHewan.length; i++) {
  jawab = urutkan.join('\n');
}
console.log(jawab);

//Soal 2

function introduction(name, age, address, hobby) {
  var data = { name: name, age: age, address: address, hobby: hobby };

  console.log(
    'Nama saya ' +
      data.name +
      ', ' +
      'Umur saya ' +
      data.age +
      ', ' +
      'Alamat saya di ' +
      data.address +
      ', ' +
      'dan saya mempunyai hobby ' +
      data.hobby
  );
}

introduction('Arief', 30, 'Jl ranca oray 67 kab.Bandung', 'Cari Uang');

//Soal 3

function hitung_huruf_vokal(vokal1, vokal2) {
  var input1 = vokal1.split('');
  var lower = input1.map(function (item) {
    return item.toLowerCase();
  });
  var simpan = 0;

  for (var i = 0; i < lower.length; i++) {
    switch (lower[i]) {
      case 'a':
        simpan += 1;
        break;
      case 'i':
        simpan += 1;
        break;
      case 'u':
        simpan += 1;
        break;
      case 'e':
        simpan += 1;
        break;
      case 'o':
        simpan += 1;
    }
  }
  return simpan;
}

var hitung_1 = hitung_huruf_vokal('Muhammad');
var hitung_2 = hitung_huruf_vokal('Iqbal');
console.log(hitung_1, hitung_2);

//Soal 4

function hitung(n) {
  let hasilDeret;
  for (i = 0; i <= n; i++) {
    if (i == 0) {
      hasilDeret = -2;
    } else {
      hasilDeret += 2;
    }
  }
  return hasilDeret;
}
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
