//Soal 1
var pertama = 'saya sangat senang hari ini';
var kedua = 'belajar javascript itu keren';

var word1 = pertama.substr(0, 4);
var word2 = pertama.substr(12, 6);
var word3 = kedua.substring(0, 7);
var word4 = kedua.substring(8, 18).toUpperCase();

console.log(
  word1
    .concat(' ')
    .concat(word2)
    .concat(' ')
    .concat(word3)
    .concat(' ')
    .concat(word4)
);

//Soal 2
var kataPertama = '10';
var kataKedua = '2';
var kataKetiga = '4';
var kataKeempat = '6';

var angka1 = parseInt(kataPertama);
var angka2 = parseInt(kataKedua);
var angka3 = parseInt(kataKetiga);
var angka4 = parseInt(kataKeempat);

console.log((angka1 + angka2) * (angka4 - angka3));

//Soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substr(4, 10);
var kataKetiga = kalimat.substr(15, 3);
var kataKeempat = kalimat.substr(19, 5);
var kataKelima = kalimat.substring(25);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);
